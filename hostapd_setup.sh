#!/bin/bash - 
#===============================================================================
#
#          FILE: hostapd_setup.sh
# 
#         USAGE: ./hostapd_setup.sh in Router.sh
# 
#   DESCRIPTION: Configuration for wifi
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Julius Espinoza jespinoza7@my.bcit.ca
#  ORGANIZATION: BCIT STUDENT
#       CREATED: May 23, 2017 12:00
#      REVISION:  ---
#===============================================================================
set -o nounset                              # Treat unset variables as an error
source variables.sh

systemctl enable hostapd
systemctl start hostapd

echo "************************************"
echo "Hostapd configuration"
cp /dev/null $HostapdConfig/hostapd.conf
cat > $HostapdConfig/hostapd.conf <<-EOF
ctrl_interface=/var/run/hostapd
ctrl_interface_group=wheel

# Most modern wireless drivers in the kernel need driver=nl80211
driver=nl80211

# Customize these for your local configuration...
interface=$Device3
driver=nl80211
hw_mode=g
channel=6
ssid=$ESSID
macaddr_acl=0
ignore_broadcast_ssid=0
auth_algs=1

#Password Authentication
wpa=3
wpa_passphrase=$Password
wpa_key_mgmt=WPA-PSK
wpa_pairwise=TKIP
rsn_pairwise=CCMP

EOF
echo "Successfully configured!"
echo "************************************"
echo ""

systemctl restart hostapd
