#!/bin/bash - 
#===============================================================================
#
#          FILE: network_setup.sh
# 
#         USAGE: To be sourced in Router.sh 
# 
#   DESCRIPTION: Configures the Network Settings of the machine. Including the interfaces and
#                services related to the network like firewall and Network Manager
# 
#       OPTIONS: ---
#  REQUIREMENTS: must be sourced in main.sh
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Julius Espinoza jespinoza7@my.bcit.ca
#  ORGANIZATION: BCIT STUDENT
#       CREATED: May 23, 2017 12:00
#      REVISION:  ---
#===============================================================================
set -o nounset                              # Treat unset variables as an error
#declare Path='/etc/sysconfig/network-scripts/ifcfg'
source variables.sh
#source netif_func.sh

echo "Turning off and disabling Network Manager"
systemctl stop NetworkManager.service
systemctl disable NetworkManager.service

source netif_func.sh

ifdown eth2
rm /etc/sysconfig/network-scripts/ifcfg-eth2

cp /dev/null /etc/resolv.conf
cat >> /etc/resolv.conf <<-EOF
nameserver 127.0.0.1
EOF

echo "Turning off and disabling Firewall Daemon"
systemctl stop firewalld.service
systemctl disable firewalld.service

echo "Enabling and starting the Network Service"
systemctl enable network.service 
systemctl start network.service
