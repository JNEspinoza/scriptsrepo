#!/bin/bash

#Flush any existing rules
iptables -F

#Default Policy Drop
iptables -t filter -P INPUT DROP
iptables -t filter -P FORWARD DROP

#----------INPUT----------#

#Allow Loopback
iptables -A INPUT -i lo -j ACCEPT

#Allow SSH on router
iptables -A INPUT -i eth0 -p tcp -m tcp --dport 22 -m state --state NEW,ESTABLISHED -j ACCEPT

#Allow ICMP traffic on the router.
iptables -A INPUT -p icmp -j ACCEPT

#Allow OSPF traffic
iptables -A INPUT -i eth0 -d 224.0.0.5 -p ospf -j ACCEPT
iptables -A INPUT -i eth0 -d 224.0.0.6 -p ospf -j ACCEPT

#Allow established connections
iptables -A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT

#Allow DNS Traffic TCP and UDP 
iptables -A INPUT -p udp -m udp --dport 53 -j ACCEPT
iptables -A INPUT -p tcp -m tcp --dport 53 -j ACCEPT

#Log
iptables -A INPUT -j LOG

#----------FORWARD----------#

#Allow established connections
iptables -A FORWARD -m state --state RELATED,ESTABLISHED -j ACCEPT

#Allow all traffic from external network to student network
iptables -A FORWARD -i eth1 -o eth0 -j ACCEPT

#Allow incoming traffic to student network
iptables -A FORWARD -i eth0 -d 10.16.4.0/25 -j ACCEPT

#----------OUTPUT----------#

#Allow traffic going out of the router
iptables -A OUTPUT -j ACCEPT

service iptables save
