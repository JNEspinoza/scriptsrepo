#!/bin/bash -
#===============================================================================
#
#          FILE: dovecot_setup.sh
# 
#         USAGE: To be sourced in Mail.sh
# 
#   DESCRIPTION: Configures different files for dovecot service
# 
#       OPTIONS: ---
#  REQUIREMENTS: must be sourced in main.sh
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Julius Espinoza jespinoza7@my.bcit.ca
#  ORGANIZATION: BCIT STUDENT
#       CREATED: June 3, 2017 12:00
#      REVISION:  ---
#===============================================================================
set -o nounset                              # Treat unset variables as an error 

yum -y install dovecot
systemctl start dovecot
systemctl enable dovecot

#--------------Dovecot---------------------#

#Configure Dovecot protocols by modifying by editing the main Dovecot configuration file 
#/etc/dovecot/dovecot.cf and specifying the following protocols

sed -i '/protocols = imap pop3 lmtp/s/^#//' /etc/dovecot/dovecot.conf

#Edit the /etc/dovecot/conf.d/10-mail.conf to specify the mail 
#location by uncommenting the following line: mail_location = maildir:~/Maildir

sed -i '/mail_location = maildir:~\/Maildir/s/^#//' /etc/dovecot/conf.d/10-mail.conf

#Allow plaintext authentication by editing /etc/dovecot/conf.d/10-auth.conf 
#and change the following line to state: disable_plaintext_auth = no

sed -i '/disable_plaintext_auth = yes/s/#//;s/yes/no/' /etc/dovecot/conf.d/10-auth.conf


systemctl restart dovecot.service
