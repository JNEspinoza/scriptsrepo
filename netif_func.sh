#!/bin/bash - 
#===============================================================================
#
#          FILE: netif_func.sh
# 
#         USAGE: sourced in network_setup.sh
# 
#   DESCRIPTION: All other scripts will be sourced by in this script.
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Julius Espinoza jespinoza7@my.bcit.ca
#  ORGANIZATION: BCIT STUDENT
#       CREATED: May 23, 2017 12:00
#      REVISION:  ---
#===============================================================================
set -o nounset                              # Treat unset variables as an error
source variables.sh

function SetEth0 {
 #/etc/sysconfig/network-scripts/ifcfg-$1
#$1 - Device
#$2 - Network ID
#$3 - IP Address
#$4 - Gateway
#$5 - Prefix
if [ $# -eq 5 ]; then
echo "************************************"
echo "Setting the static IP for eth0"
cp /dev/null $IntConfigPath-$1
cat > $IntConfigPath-$1 <<-EOF
TYPE=Ethernet
BOOTPROTO=none
NM_CONTROLLED=no
DEFROUTE=yes
IPV4_FAILURE_FATAL=no
NAME=$1
DEVICE=$1
ONBOOT=yes
IPADDR=$2.$3
PREFIX=$5
GATEWAY=$2.$4
DNS1=127.0.0.1
EOF
ifdown $1
ifup $1
echo "Successfully configured!"
echo "************************************"
echo ""
else
  echo "Process Failed!"
fi
}

function SetEth1 {
#/etc/sysconfig/network-scripts/ifcfg-$1
#Sourcing Eth1 Config setup with 3 needed arguments
#$1 - Name
#$2 - IP Address
#$3 - Prefix
if [ $# -eq 3 ]; then
echo "************************************"
echo "Setting the static IP for eth1"
cp /dev/null $IntConfigPath-$1
cat > $IntConfigPath-$1 <<-EOF
TYPE=Ethernet
BOOTPROTO=none
NM_CONTROLLED=no
DEFROUTE=yes
IPV4_FAILURE_FATAL=no
NAME=$1
DEVICE=$1
ONBOOT=yes
IPADDR=$2
PREFIX=$3
EOF
ifdown $1
ifup $1
echo "Successfully configured!"
echo "************************************"
echo ""
else
  echo "Process Failed!"
fi

}

function SetWireless {
#/etc/sysconfig/network-scripts/ifcfg-$1
#Sourcing Eth1 Config setup with 4 needed arguments
#$1 - Name
#$2 - IP Address
#$3 - Prefix
#$4 - ESSID
if [ $# -eq 4 ]; then
echo "************************************"
echo "Setting the static IP for Wireless Interface"
cp /dev/null $IntConfigPath-$1
cat > $IntConfigPath-$1 <<-EOF
DEVICE=$1
BOOTPROTO=none
TYPE=Wireless
ONBOOT=yes
NM_CONTROLLED=no
IPADDR=$2
PREFIX=$3
ESSID=$4
CHANNEL=6
MODE=Master
RATE=Auto
EOF
ifdown $1
ifup $1
echo "Successfully configured!"
echo "************************************"
else
  echo "Process Failed!"
fi
}



#Exec Functions
SetEth0 $Device1 $NetID $Eth0Address $Gateway $Eth0Prefix
SetEth1 $Device2 $StudentNetIP $StudentNetPrefix
SetWireless $Device3 $WirelessIP $StudentNetPrefix $ESSID
