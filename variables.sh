#!/bin/bash - 
#===============================================================================
#
#          FILE: variables.sh
# 
#         USAGE: sourced in every script.
# 
#   DESCRIPTION: All variables for the scripts. Sourced in every script
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Julius Espinoza jespinoza7@my.bcit.ca
#  ORGANIZATION: BCIT STUDENT
#       CREATED: May 23, 2017 12:00
#      REVISION:  ---
#===============================================================================
set -o nounset                              # Treat unset variables as an error

#Mail or Router
declare InstallVm

#Hostname
declare HostName=s04rtr.as.learn
declare MailHostName=mail.s04.as.learn
declare PrefixHostname=$(echo $HostName | cut -c4-6 )

#Paths
declare IntConfigPath='/etc/sysconfig/network-scripts/ifcfg'
declare QuaggaConfig='/etc/quagga'
declare DhcpConfig='/etc/dhcp'
declare UnboundConfig='/etc/unbound'
declare NsdConfig='/etc/nsd'
declare HostapdConfig='/etc/hostapd'

#Variables for eth0
declare Device1=eth0
declare NetID=10.16.255
declare Eth0Address=4
declare Gateway=254
declare Eth0Prefix=24

#Variables for eth1
declare Device2=eth1
declare StudentNetIP=10.16.4.126
declare StudentNetPrefix=25
declare StudentNetID1=10.16.4.0
declare MailIP=10.16.4.1

#Variables for Wireless
declare Device3=wlp0s6u1
declare WirelessIP=10.16.4.254
declare ESSID=NASP19_S04
declare StudentNetID2=10.16.4.128
declare Password=Password123

#DHCP and DNS Variables
declare DomainName=s04.as.learn
declare BinNetMask=255.255.255.128
declare Range1="10.16.4.100   10.16.4.125"
declare Range2="10.16.4.129   10.16.4.253"
declare ReverseDomain=4.16.10.in-addr.arpa

#Mail Variables
declare MailEth=eth0
declare MailIP=10.16.4.1


