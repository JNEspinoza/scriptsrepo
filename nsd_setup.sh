#!/bin/bash - 
#===============================================================================
#
#          FILE: nsd_setup.sh
# 
#         USAGE: ./nsd_setup.sh
# 
#   DESCRIPTION: Creates nsd.conf and zone files
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Julius Espinoza jespinoza7@my.bcit.ca
#  ORGANIZATION: BCIT STUDENT
#       CREATED: May 23, 2017 12:00
#      REVISION:  ---
#===============================================================================
set -o nounset                              # Treat unset variables as an error
source variables.sh

#Start and Enable NSD Service
systemctl start nsd
systemctl enable nsd

echo "Setting the config file for NSD service"
echo "************************************"
echo "Making nsd.conf"
cp /dev/null $NsdConfig/nsd.conf
cat > $NsdConfig/nsd.conf <<-EOF
#
#nsd.conf -- the NSD(8) configuration file, nsd.conf(5).
#
# Copyright (c) 2001-2011, NLnet Labs. All rights reserved.
#
# See LICENSE for the license.
#
# Simplified version for NASP

# options for the nsd server
server:
        # uncomment to specify specific interfaces to bind (default are the
        # wildcard interfaces 0.0.0.0 and ::0).
        # For servers with multiple IP addresses, list them one by one,
        ip-address: $NetID.$Eth0Address
	    #ip-address: 127.0.0.1

        # listen on IPv4 connections - yes is the default left commented
        # do-ip4: yes

        # listen on IPv6 connections
        do-ip6: no

        # port to answer queries on. default is 53 left commented
         port: 53

        # After binding socket, drop user privileges.
        # can be a username, id or id.gid. 
        # when looking for running processes nsd will be running as this user
        # username: nsd

        # The directory for zonefiles.
        # Use default, left commented
        #
        #zonesdir: "/etc/nsd"

        # Optional local server config
        include: "/etc/nsd/server.d/*.conf"

        # Include optional local configs.
        include: "/etc/nsd/conf.d/*.conf"

# Remote control config section.
remote-control:
        # Enable remote control with nsd-control(8) here.
        # set up the keys and certificates with nsd-control-setup.
        control-enable: yes

        # what interfaces are listened to for control, default is on localhost.
        # use default leave commented
        #
        # control-interface: 127.0.0.1
        # control-interface: ::1

        # port number for remote control operations (uses TLS over TCP).
        # use default leave commented
        #
        # control-port: 8952

        # nsd server key file for remote control.
        # use default leave commented
        #
        # server-key-file: "/etc/nsd/nsd_server.key"

        # nsd server certificate file for remote control.
        # use default leave commented
        #
        # server-cert-file: "/etc/nsd/nsd_server.pem"

        # nsd-control key file.
        # use default leave commented
        #
        # control-key-file: "/etc/nsd/nsd_control.key"

        # nsd-control certificate file.
        # use default leave commented
        #
        # control-cert-file: "/etc/nsd/nsd_control.pem"

# Fixed zone entries (i.e. not added with nsd-control addzone)

zone:
        name: "$DomainName"
        zonefile: "$DomainName.zone"

zone:
        name: "$ReverseDomain"
        zonefile: "$ReverseDomain.zone"
EOF
echo "Successfully configured!"
echo "************************************"
echo ""
echo "************************************"
echo "Creating the zone files."
echo "Setting the zone file for $DomainName.zone"
cp /dev/null $NsdConfig/$DomainName.zone
cat > $NsdConfig/$DomainName.zone <<-EOF
;zone file for $DomainName
\$TTL 10s                              ; 10 secs default TTL for zone
$DomainName.   IN  SOA   $HostName. htp.bcit.ca. (
                        2014022521    ; serial number of Zone Record
                        1200s         ; refresh time
                        180s          ; update retry time on failure
                        1d            ; expiration time 
                        3600          ; cache time to live
                        )

;Name servers for this domain
$DomainName.         IN      NS     $HostName.

;addresses of hosts
$PrefixHostname.$DomainName.     IN      A      $StudentNetIP
$MailHostName.         	   IN      A      $MailIP
$DomainName.               IN MX   10     $MailHostName.
EOF
echo "Successfully configured!!"
echo "************************************"
echo ""
echo "************************************"
echo "Setting the zone file for 4.16.10.in-addr.arpa.zone"
cp /dev/null $NsdConfig/$ReverseDomain.zone
cat > $NsdConfig/$ReverseDomain.zone <<-EOF
;zone file for $ReverseDomain
\$TTL 10s                              ; 10 secs default TTL for zone
$ReverseDomain.   IN SOA  $HostName. htp.bcit.ca. (
                        2019022731    ; serial number of Zone Record
                        1200s         ; refresh time
                        180s          ; update retry time on failure
                        1d            ; expiration time 
                        3600          ; cache time to live
                        )

;Name servers for this domain
$ReverseDomain.         IN      NS     $HostName.


;addresses of hosts
126.$ReverseDomain.   IN  PTR $HostName.$DomainName.
1.$ReverseDomain.     IN  PTR $MailHostName.
EOF
#echo "$TestPath/$DomainName.zone"
echo "Successfully configured!!"
echo "************************************"

#Restart the nsd service
systemctl restart nsd
