#!/bin/bash - 
#===============================================================================
#          FILE: quagga_setup.sh
#         USAGE: ./quagga_setup.sh
#   DESCRIPTION: Configures quagga and source the config files from zebra_setup.sh
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Julius Espinoza jespinoza7@my.bcit.ca
#  ORGANIZATION: BCIT STUDENT
#       CREATED: May 23, 2017 12:00
#      REVISION:  ---
#===============================================================================
set -o nounset                              # Treat unset variables as an error
source variables.sh

#Start and Enable ospfd.services
systemctl start ospfd.service
systemctl enable ospfd.service

#Start and Enable zebra.services
systemctl start zebra.service
systemctl enable zebra.service

source zebra_setup.sh

chown quagga:quagga $QuaggaConfig/zebra.conf
chown quagga:quagga $QuaggaConfig/ospfd.conf

#Start and Enable zebra.services and osfd.service
systemctl restart zebra.service
systemctl restart ospfd.service







