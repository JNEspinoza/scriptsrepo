#!/bin/bash -
set -o nounset                              # Treat unset variables as an error 

source variables.sh

function SetEth0 {
 #/etc/sysconfig/network-scripts/ifcfg-$1
#$1 - Device
#$2 - Network ID
#$3 - IP Address
#$4 - Gateway
#$5 - Prefix
if [ $# -eq 4 ]; then
echo "************************************"
echo "Setting the static IP for eth0"
cp /dev/null $IntConfigPath-$1
cat >> $IntConfigPath-$1 <<-EOF
TYPE=Ethernet
BOOTPROTO=none
NM_CONTROLLED=no
DEFROUTE=yes
IPV4_FAILURE_FATAL=no
NAME=$1
DEVICE=$1
ONBOOT=yes
IPADDR=$2
PREFIX=$3
GATEWAY=$4
EOF
ifdown $1
ifup $1
echo "Successfully configured!"
echo "************************************"
echo ""
else
  echo "Process Failed!"
fi
}

#---------------------------------------------------------------------------------------------#

echo "Turning off and disabling Network Manager"
systemctl stop NetworkManager.service
systemctl disable NetworkManager.service

SetEth0 $MailEth $MailIP $StudentNetPrefix $StudentNetIP

echo "Turning off and disabling Firewall Daemon"
systemctl stop firewalld.service
systemctl disable firewalld.service

echo "Enabling and starting the Network Service"
systemctl enable network.service 
systemctl start network.service


./postfix_setup.sh
./dovecot_setup.sh
