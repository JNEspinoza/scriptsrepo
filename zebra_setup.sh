#!/bin/bash - 
#===============================================================================
#
#          FILE: quagga_setup.sh
# 
#         USAGE: sourced in quagga_setup.sh
# 
#   DESCRIPTION: Setup zebra config file and ospfd.conf
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Julius Espinoza jespinoza7@my.bcit.ca
#  ORGANIZATION: BCIT STUDENT
#       CREATED: June 1, 2017 12:00
#      REVISION:  ---
#===============================================================================
set -o nounset                              # Treat unset variables as an error
source variables.sh

echo "Setting the config file for quagga"
echo "************************************"
echo "Making Zebra.conf"
cp /dev/null $QuaggaConfig/zebra.conf
cat >> $QuaggaConfig/zebra.conf <<-EOF
!
! Zebra configuration saved from Script
!
hostname $HostName
log file /var/log/quagga/quagga.log
!
interface $Device1
 description $Device1
 ip address $NetID.$Eth0Address/$Eth0Prefix
 ipv6 nd suppress-ra
!
interface $Device2
 description $Device2
 ip address $StudentNetIP/$StudentNetPrefix
 ipv6 nd suppress-ra
!
interface lo
!
interface $Device3
 description $Device3
 ip address $WirelessIP/$StudentNetPrefix
 ipv6 nd suppress-ra
!
router-id $NetID.$Eth0Address
ip forwarding
!
!
line vty
!
EOF
echo "Successfully configured!!"
echo "************************************"

echo "************************************"
echo "Making ospfd.conf"
cp /dev/null $QuaggaConfig/ospfd.conf
cat >> $QuaggaConfig/ospfd.conf <<-EOF
!
! Zebra configuration from script
!
hostname ospfd
password zebra
log stdout
!
!
!
interface $Device1
!
interface $Device2
!
interface lo
!
interface $Device3
 description wireless
!
router ospf
 ospf router-id $NetID.$Eth0Address
 network $StudentNetID1/$StudentNetPrefix area 0.0.0.0
 network $StudentNetID2/$StudentNetPrefix area 0.0.0.0
 network $NetID.$Eth0Address/$Eth0Prefix area 0.0.0.0
!
line vty
!
EOF
echo "Successfully configured!!"
echo "************************************"

