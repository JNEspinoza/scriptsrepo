#!/bin/bash - 
#===============================================================================
#
#          FILE: base_configuration.sh
# 
#         USAGE: To be sourced in main.sh 
# 
#   DESCRIPTION: Configures the base configuration of the system
# 
#       OPTIONS: ---
#  REQUIREMENTS: must be sourced in main.sh
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Julius Espinoza jespinoza7@my.bcit.ca
#  ORGANIZATION: BCIT STUDENT
#       CREATED: May 23, 2017 12:00
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error
source variables.sh

#The basic installation of CentOS Linux
echo "Installing base packages"
yum -y update

#Tells yum which type of packages in groups will be installed when 'groupinstall' is called. 
#Default is: default, mandatory
echo "group_package_types=mandatory,default,optional" >> /etc/yum.conf
yum -y group install base

#Install EPEL (Extra Packages for Enterprise Linux)
echo "Installing the Extra Packages for Enterprise Linux Repository" 
yum -y install epel-release
yum -y update

#Installation of the following tools:
#Curl - transder a URL
#vim  - text editor
#wget - non-interactive network downloader.
#tmux - terminal multiplexer
#nmap - Network exploration tool and security / port scanner
#ncat - Concatenate and redirect sockets
#tcpdump - dump traffic on a network
#git - the stupid content tracker
echo "Installing project specific tools"
yum -y install curl vim wget tmux nmap ncat tcpdump git

#kernel-devel - Development package for building kernel modules to match
#kernel-headers - Header files for the Linux kernel for use by glibc
#dkms - Dynamic Kernel Module Support Framework
#gcc - Various compilers (C, C++, Objective-C, Java, ...)
#gcc-c+
echo "Installing pre-requisities"
yum -y install kernel-devel kernel-headers dkms gcc gcc-c+


#Install Quagga
yum -y install quagga dhcp unbound nsd hostapd iptable
yum -y install iptables-services

#Set Hostname to s17rtr.as.learn
hostnamectl set-hostname $HostName








