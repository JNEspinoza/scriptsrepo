#!/bin/bash -
#===============================================================================
#
#          FILE: main.sh
# 
#         USAGE: Configures postfix config files.
# 
#   DESCRIPTION: All other scripts will be sourced by in this script.
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Julius Espinoza jespinoza7@my.bcit.ca
#  ORGANIZATION: BCIT STUDENT
#       CREATED: May 23, 2017 12:00
#      REVISION:  ---
#===============================================================================
set -o nounset                              # Treat unset variables as an error 

yum -y install postfix

systemctl start postfix
systemctl enable postfix

#--------------Postfix---------------------#
#Update the line that starts with inet_interfaces to read:
sed -i '/inet_interfaces = all/s/^#//g' /etc/postfix/main.cf
sed -i '/inet_interfaces = localhost/s/^/#/g' /etc/postfix/main.cf

#Update The email destination
sed -i 's/^mydestination = $myhostname, localhost.$mydomain, localhost\b/&, $mydomain/' /etc/postfix/main.cf

#Uncomment the line: home_mailbox = Maildir/
sed -i '/home_mailbox = Maildir/s/^#//g' /etc/postfix/main.cf

systemctl restart postfix



