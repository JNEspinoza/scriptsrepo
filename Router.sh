#!/bin/bash -
#===============================================================================
#
#          FILE: Router.sh
# 
#         USAGE: bash Router.sh
# 
#   DESCRIPTION: All config files for the Router are run here.
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Julius Espinoza jespinoza7@my.bcit.ca
#  ORGANIZATION: BCIT STUDENT
#       CREATED: May 23, 2017 12:00
#      REVISION:  ---
#===============================================================================
set -o nounset                              # Treat unset variables as an error
./base_configuration.sh
./network_setup.sh
./quagga_setup.sh
./dhcpd_setup.sh
./nsd_setup.sh
./unbound_setup.sh
./hostapd_setup.sh
./iptables_setup.sh
reboot
shutdown
reboot
