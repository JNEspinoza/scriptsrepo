#!/bin/bash - 
#===============================================================================
#
#          FILE: dhcp_setup.sh
# 
#         USAGE: ./main.sh
# 
#   DESCRIPTION: Dhcp configuration
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Julius Espinoza jespinoza7@my.bcit.ca
#  ORGANIZATION: BCIT STUDENT
#       CREATED: May 23, 2017 12:00
#      REVISION:  ---
#===============================================================================
set -o nounset                              # Treat unset variables as an error
source variables.sh

systemctl start dhcpd
systemctl enable dhcpd

echo "Setting the config file for DHCP service"
echo "************************************"
echo "Making dhcpd.conf"
cp /dev/null $DhcpConfig/dhcpd.conf
cat > $DhcpConfig/dhcpd.conf <<-EOF
#
# Just to distinguish
# DHCP Server Configuration file.
#   see /usr/share/doc/dhcp*/dhcpd.conf.example
#   see dhcpd.conf(5) man page
#
default-lease-time 600;
max-lease-time 7200;
option domain-name-servers $StudentNetIP;
option domain-name "$DomainName";

subnet $StudentNetID1  netmask $BinNetMask {
  range $Range1;
  option routers    $StudentNetIP;
}

subnet $StudentNetID2 netmask $BinNetMask {
  range $Range2;
  option routers    $WirelessIP;
}
EOF
echo "Successfully configured!"
echo "************************************"


systemctl restart dhcpd
